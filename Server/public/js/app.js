const PROTOCOL_MAP = {
    'http:': 'ws://',
    'https:': 'wss://'
};

const SOCK_PATH =
    PROTOCOL_MAP[window.location.protocol] + window.location.host + '/socket';
const SOCK = new WebSocket(SOCK_PATH);

function setKey(x, y) {
    let color = document.getElementById('color').value;
    SOCK.send(JSON.stringify(
        {'type': 'color', 'data': {'key': [x, y], 'val': color}}));
};

SOCK.onmessage = (evt) => {
    let data = JSON.parse(evt.data);
    if (data.type == 'update') {
        for (row in data.data) {
            for (key in data.data[row]) {
                let el = document.getElementById(row + '.' + key)
                el.style.background = data.data[row][key];
            }
        }
    }
};

SOCK.onopen = (evt) => {
    setInterval(() => {SOCK.send('ping')}, 30 * 1000);
};
