#!/usr/bin/env python3
import json
import asyncio
import sys

import websockets
import openrazer.client as rc

dm = rc.DeviceManager()
kb = dm.devices[0]

def hex_to_rgb(color):
    return (
            int(color[1:3], 16),
            int(color[3:5], 16),
            int(color[5:], 16)
        )

async def handler():
    async with websockets.connect("wss://" + sys.argv[1] + "/socket") as websocket:
        while True:
            try:
                message = await asyncio.wait_for(websocket.recv(), timeout=30)
            except asyncio.TimeoutError:
                try:
                    p_w = await websocket.ping()
                    await asyncio.wait_for(p_w, timeout=10)
                except asyncio.TimeoutError:
                    print("Server has left us :(")
                    sys.exit(0)
            d = json.loads(message)
            if d["type"] == "update":
                for y, r in enumerate(d["data"]):
                    for x, c in enumerate(r):
                        kb.fx.advanced.matrix[y, x + 1] = hex_to_rgb(c)
                kb.fx.advanced.draw()

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage: %s <domain>" % sys.argv[0])
        sys.exit(1)
    asyncio.get_event_loop().run_until_complete(handler())
